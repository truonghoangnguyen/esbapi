from flask import jsonify
import cx_Oracle
import config
from common import oracle_connection, connection_pool, exception

from flask import request

def accounts_available_balance(connection , accounts_para):
    print(request.path)
    if accounts_para is None:
        raise exception.ESBBadRequest(config.BAD_REQUEST_ERROR, "ESBBadRequest")

    accounts = accounts_para.split(",")
    if len(accounts) > 120:
        raise exception.ESBBadRequest(config.BAD_REQUEST_ERROR, "array size limit exceeded",)


    if not accounts or len(accounts) == 0:
        return []


    account_results = []


    cursor = connection.cursor()

    # get date
    sql="SELECT nvl((select b.bankoptionvalue from bankoption b where b.bankoptioncd = 'CDAT' and b.bankoptiononyn = 'Y'), \
            (SELECT t.bankoptionvalue from bankoption t where t.bankoptioncd = 'PDAT')) from dual"
    cursor.execute(sql)
    # results = cursor.fetchall()
    # for row in results:
    #     in_effdate = row[0]
    
    row = cursor.fetchone()
    in_effdate = row[0]


    # loop all accounts
    for account in accounts:
        out_acctbalamt = cursor.var(float)
        out_acctavailamt = cursor.var(float)
        out_acctmgmtholdamt = cursor.var(float)
        out_acctavailholdamt = cursor.var(float)
        out_totdrawamt = cursor.var(float)
        out_totcreditavailamt = cursor.var(float)
        out_totcreditlimitamt = cursor.var(float)
        out_errornbr = cursor.var(str)
        out_errormsg = cursor.var(str)
        out_oraerrormsg = cursor.var(str)

        in_acctnbr = account #
        # in_notesubacctnbr = 'BAL' #notesubacctnbr # 'BAL'
        in_subacctnbr = -1 # 1
        in_availmethcd = 'ONLI'
        in_debugyn = 'N'

        # sql="SELECT SUBACCTNBR FROM ACCTSUBACCT WHERE ACCTNBR = %s AND BALCATCD = 'NOTE' AND BALTYPCD ='BAL'"%account
        sql="SELECT SUBACCTNBR FROM ACCTSUBACCT WHERE ACCTNBR = :account AND BALCATCD = 'NOTE' AND BALTYPCD ='BAL'"
        cursor.execute(sql,{"account":account})
        # results = cursor.fetchall()
        # for row in results:
        #     in_subacctnbr = row[0]

        row = cursor.fetchone()
        if row is None:
            raise exception.ESBNotFound(404, "SUBACCTNBR not found for account %s"%account)
        in_subacctnbr = row[0]
        
        cursor.callproc('PROC_AVAILFUNDS', (in_acctnbr, in_subacctnbr, in_effdate, in_availmethcd,  in_debugyn, \
            out_acctbalamt, \
            out_acctavailamt, \
            out_acctmgmtholdamt, \
            out_acctavailholdamt, \
            out_totdrawamt, \
            out_totcreditavailamt, \
            out_totcreditlimitamt, \
            out_errornbr, \
            out_errormsg, \
            out_oraerrormsg))

        m_errornbr = out_errornbr.getvalue()
        if m_errornbr is None:
            m_errornbr = ''

        if len(m_errornbr) > 0  :
            msg = "account not found: %s" % (account)
            raise exception.ESBNotFound(int(m_errornbr), msg)

        ret = {
            'acctNbr': account,
            'acctBalAmt': round(out_acctbalamt.getvalue(), 2),
            'acctAvailAmt': round(out_acctavailamt.getvalue(), 2),
            # 'AcctMgmtHoldAmt': round(out_acctmgmtholdamt.getvalue(), 2),
            # 'AcctAvailHoldAmt': round(out_acctavailholdamt.getvalue(), 2),
            # 'TotDrawAmt': round(out_totdrawamt.getvalue(), 2),
            # 'TotCreditAvailAmt': round(out_totcreditavailamt.getvalue(), 2),
            # 'TotCreditLimitAmt': round(out_totcreditlimitamt.getvalue(), 2)
            }
        account_results.append(ret)


    cursor.close()

    return account_results

