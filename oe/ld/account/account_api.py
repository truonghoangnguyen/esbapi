from oe.ld.account import account_availbal
from oe.ld.account import accounts_availbal
from oe.ld.account import passbook_availbal
from oe.ld.account import account_tnx
from flask import make_response, jsonify, request, Blueprint
from common.api_handle import oracedb_rest_api_wrapper
import config
from common import exception
from common.cache import app_cache
from flasgger import swag_from

""" main entry handle for account api
"""

account_api = Blueprint('account_api', __name__)

@account_api.route('/oe/ld/accounts/<int:accountnbr>')
@app_cache.cached(timeout=config.CACHE_TIMEOUT, query_string=True)
@swag_from ('/docs/account_available_balance.yml')
def account_available_balance(accountnbr):
    """ get one account balance (full info) """
    # day la cach di tu quan ly pool trong port
    # return simple_rest_api_wrapper(account_availbal.account_available_balance_port, accountnbr)
    # return dna_rest_api_wrapper(account_availbal.account_available_balance, accountnbr)
    return oracedb_rest_api_wrapper(account_availbal.account_available_balance, accountnbr)

@account_api.route('/oe/ld/accounts/')
@app_cache.cached(timeout=config.CACHE_TIMEOUT, query_string=True)
def accounts_available_balance():
    """ get list account balance (available + total amount)"""
    accounts = request.args.get('accounts')
    # result = dna_rest_api_wrapper(accounts_availbal.accounts_available_balance, accounts)
    result = oracedb_rest_api_wrapper(accounts_availbal.accounts_available_balance, accounts)

    return result

# chu y: ten ben o day phai khop voi ngay bao cho route tren file app.py o day la bien 'passbook'
# http://localhost:5000/oe/ld/passbooks/48745329/?serial_number=A059316
@account_api.route('/oe/ld/passbooks/<int:accountnbr>/')
@app_cache.cached(timeout=config.CACHE_TIMEOUT, query_string=True)
def account_passbook_available(accountnbr):
    """ get one account balance (full info)"""
    serial_number = request.args.get('serial_number')
    return oracedb_rest_api_wrapper(passbook_availbal.account_passbook_available, accountnbr, serial_number)

# vay http://127.0.0.1:5000/oe/ld/accounts/161072989/transactions/?fromdate=2014-01-01&todate=2018-10-01
#http://127.0.0.1:5000/oe/ld/accounts/73839719/transactions/?fromdate=2018-01-01&todate=2018-10-01
@account_api.route('/oe/ld/accounts/<int:accountnbr>/transactions/')
@app_cache.cached(timeout=config.CACHE_TIMEOUT, query_string=True)
@swag_from ('/docs/account_available_balance.yml')
def account_transactions_hist(accountnbr):
    """ get one account balance (full info) """
    # day la cach di tu quan ly pool trong port
    # return simple_rest_api_wrapper(account_availbal.account_available_balance_port, accountnbr)
    # return dna_rest_api_wrapper(account_availbal.account_available_balance, accountnbr)

    fromdate = request.args.get('fromdate')
    todate = request.args.get('todate')
    rtxnref = request.args.get('rtxnref')
    page = request.args.get('page')
    size = request.args.get('size')

    return oracedb_rest_api_wrapper(account_tnx.account_transactions_hist, accountnbr, fromdate, todate,rtxnref,page,size )
