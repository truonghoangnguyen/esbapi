from flask import jsonify
import cx_Oracle
import config
from common import exception
from common import oracle_connection, connection_pool, exception
import json
from common.sql_tool import sqltojson,sqltojson_one

def  account_transactions_hist(connection, accountnbr,fromdate, todate, rtxnref, page, size):
    in_page = 1
    in_size = 20
    if page is not None:
        print('page:' + page)
        in_page = int(page)
    if size is not None:
        print('size:' + size)
        in_size =int(size)

    if in_page <1 or  in_size<1:
        raise exception.ESBNotFound(config.BAD_REQUEST_ERROR, "ESBBadRequest")

    # if fromdate is None or todate is None:
    #     raise exception.ESBBadRequest(config.BAD_REQUEST_ERROR, "ESBBadRequest")
    print('fromdate1111:' + str(fromdate))
    print('todate22222:' + str(todate))
    #2018-01-01
    transactions_results = []

    #abh.acctnbr \"acctNbr\", abh.subacctnbr \"subAcctNbr\",
    ret = sqltojson_one(connection, "select abh.acctnbr \"acctNbr\", abh.subacctnbr \"subAcctNbr\", TO_CHAR(abh.effdate, 'YYYY-MM-DD') \"effDate\", abh.balamt \"balAmt\"\
                 from acctbalhist abh, acct a\
                where abh.acctnbr = a.acctnbr\
                and abh.subacctnbr = (select subacctnbr from acctsubacct asa\
                                    where asa.acctnbr = abh.acctnbr\
                                    and asa.balcatcd = 'NOTE'\
                                    and asa.baltypcd = 'BAL')\
                                    \
                /*-- and a.mjaccttypcd = 'CK'*/\
                and abh.effdate = (select max(abh2.effdate)\
                                           from acctbalhist abh2\
                                           where abh2.acctnbr = abh.acctnbr\
                                           and abh2.subacctnbr = abh.subacctnbr\
                                           and abh2.effdate < to_date(:fromdate,'YYYY-MM-DD'))\
                                           \
                and abh.acctnbr in (:accountnbr)\
                order by 1,3",  params={"accountnbr": str(accountnbr), "fromdate": fromdate})
    # print('sql_to_json:' + str(len(ret)))

    # print("1")
    # if len(ret) == 0:
    #     raise exception.ESBNotFound(404, "No Data Found")


    transactions_results.append({"balFromDate": ret})
    ret = sqltojson_one(connection, "select abh.acctnbr \"acctNbr\", abh.subacctnbr \"subAcctNbr\", TO_CHAR(abh.effdate, 'YYYY-MM-DD') \"effDate\", abh.balamt \"balAmt\"\
                 from acctbalhist abh, acct a\
                where abh.acctnbr = a.acctnbr\
                and abh.subacctnbr = (select subacctnbr from acctsubacct asa\
                                    where asa.acctnbr = abh.acctnbr\
                                    and asa.balcatcd = 'NOTE'\
                                    and asa.baltypcd = 'BAL')\
                                    \
                /*-- and a.mjaccttypcd = 'CK'*/\
                and abh.effdate = (select max(abh2.effdate)\
                                           from acctbalhist abh2\
                                           where abh2.acctnbr = abh.acctnbr\
                                           and abh2.subacctnbr = abh.subacctnbr\
                                           and abh2.effdate <= to_date(:todate,'YYYY-MM-DD'))\
                                           \
                and abh.acctnbr in (:accountnbr)\
                order by 1,3",  params={"accountnbr": str(accountnbr), "todate": todate})
    # print("2")
    # print('sql_to_json:' + str(len(ret)))
    # if len(ret) == 0:
    #     raise exception.ESBNotFound(404, "No Data Found")

    transactions_results.append({"balToDate": ret})












    # ret = sql_to_json(connection, "select r.acctnbr \"acctNbr\",r.rtxnnbr \"rtxnNbr\", TO_CHAR(r.origpostdate, 'DD-MM-YY') \"origPostDate\" , r.rtxntypcd \"rtxnTypCd\", r.tranamt  \"tranAmt\", t.intrtxndesctext \"descText\" \
    #         from rtxn r, intrtxndesc t\
    #         where r.acctnbr = "+str(accountnbr)+"\
    #         and t.intrtxndescnbr = r.intrrtxndescnbr\
    #         and r.currrtxnstatcd = 'C'\
    #         and r.origpostdate between to_date('"+fromdate+"','yyyy-mm-dd') and  to_date('"+todate+"','yyyy-mm-dd')  and rownum<1000 \
    #         order by r.acctnbr, r.rtxnnbr\
    #         ")


    # in_page=1, in_size=20  >>0


    row_begin=(in_page-1) * in_size
    # in_page=1, in_size=20  >>0
    row_end = in_page * in_size

    print('row_begin:' + str(row_begin))
    print('row_end:' + str(row_end))

    sql_row = " and rownum >:row_begin and rownum <=:row_end"

    retTaikhoanvay = sqltojson_one(connection, "select count(*) kt_tk_vay from acct a where a.mjaccttypcd in ('CML', 'CNS', 'MTG') and a.acctnbr =:accountnbr ", params={"accountnbr": str(accountnbr)})
    tkvay= retTaikhoanvay.get("KT_TK_VAY")
    # print(json.dumps(retTaikhoanvay))
    # print('tk:' + str(tkvay))

    sqltext = None
    params = None

    sql_rtxnref_para = ' '

    sql_rtxnref_table = ''
    sql_rtxnref_where = ''
    bl_docref= False
    if rtxnref is not None:
        tm_rtxnref=rtxnref.replace(" ","")
        if tm_rtxnref != '':
            bl_docref=True
            sql_rtxnref_para = "and re.rtxnentityattribvalue = :rtxnref "
            sql_rtxnref_table = ', rtxnentityattrib re '
            sql_rtxnref_where = " a.acctnbr = re.acctnbr(+) \
                  and a.rtxnnbr = re.rtxnnbr(+) \
                  and re.entityattribcd(+) = 'REFNBR' and "



    if tkvay >0:
        # tai khoan vay
        print('tai khoan vay')
        sqltext="select  nvl(to_char(e.rtxnnbr), ' ') AS \"rtxnNbr\", \
       thoiGianGiaoDich AS \"origPostDate\", to_char(soTien) AS \"tranAmt\", \
       decode((select count(*) \
                from rtxn x \
               where x.rtxntypcd in (select x1.rtxntypcd \
                                       from rtxntyp x1 \
                                      where x1.rtxntypcatcd in ('SC', 'MSC') \
                                     union \
                                     select 'GLR' from dual) \
                 and exists (select 1 \
                        from rtxnentityattrib x2 \
                       where x2.acctnbr = x.acctnbr \
                         and x2.rtxnnbr = x.rtxnnbr \
                         and x2.entityattribcd = 'SCRT') \
                 and x.acctnbr = e.acctnbr \
                 and x.rtxnnbr = e.rtxnnbr), \
              0, \
              (nvl(decode(instr(intrtxndesctext, '#'), \
                          0, \
                          intrtxndesctext, \
                          upper(substr(intrtxndesctext, \
                                       0, \
                                       instr(intrtxndesctext, '#') - 1) || ' \
                                                ' || \
                                substr(intrtxndesctext, \
                                       instr(intrtxndesctext, '#', 1, 2) + 1, \
                                       length(intrtxndesctext) - \
                                       instr(intrtxndesctext, '#', 1, 2) - 1) || \
                                extrtxndesctext)), \
                   ' ')), \
              ' THU PHI GIAO DICH ') AS \"descText\" \
        \
  from (select * from (select  rownum \"rowId\", a.rtxnnbr, \
               to_char(a.origpostdate, 'YYYY-MM-DD') thoiGianGiaoDich, \
               (select c.intrtxndesctext \
                  from intrtxndesc c \
                 where c.intrtxndescnbr = a.intrrtxndescnbr) as intrtxndesctext, \
               (select d.extrtxndesctext \
                  from extrtxndesc d \
                 where d.extrtxndescnbr = a.extrtxndescnbr) as extrtxndesctext, \
               (b.amt * -1) soTien, \
               a.acctnbr \
          from rtxn a, rtxnbal b " + sql_rtxnref_table + "  \
         where a.rtxnnbr = b.rtxnnbr \
           and a.acctnbr = b.acctnbr \
           and  \
           " + sql_rtxnref_where + "  \
            a.rtxntypcd not in ('RPMT', \
                                   'IAA', \
                                   'PEN', \
                                   'CHRT', \
                                   'CHAS', \
                                   'CHWV', \
                                   'GLAC', \
                                   'LTCG', \
                                   'LRCT', \
                                   'LTCW', \
                                   'LIAA', \
                                   'LIA', \
                                   'NPIA', \
                                   'NPIR', \
                                   'OLTC', \
                                   'ORCT', \
                                   'OLTW', \
                                   'UDSB', \
                                   'URCT') \
           and a.acctnbr = :accountnbr \
           and a.origpostdate >= to_date(:fromdate, 'YYYY-MM-DD') \
           and a.origpostdate <= to_date(:todate, 'YYYY-MM-DD') \
         " + sql_rtxnref_para + "  \
         and rownum <=:row_end  \
         order by a.origpostdate , a.rtxnnbr )  ) e where \"rowId\">:row_begin "

        # params= ("accountnbr":str(accountnbr), fromdate, todate)


        if bl_docref is  False:
            # print('tai khoan vay KHONG dung bl_docref')
            params = {"accountnbr": str(accountnbr), "fromdate": fromdate, "todate": todate,"row_begin": row_begin,"row_end": row_end}
        else:
            # print('tai khoan vay dung bl_docref')
            params = {"accountnbr": str(accountnbr), "fromdate": fromdate, "todate": todate, "rtxnref": rtxnref,"row_begin": row_begin,"row_end": row_end}


    else:
        # tai khoan thuong
        print('tai khoan thuong')
        sqltext ="select  rtxnnbr \"rtxnNbr\", \
       thoiGianGiaoDich AS  \"origPostDate\", \
       to_char(soTien) AS \"tranAmt\", \
       decode((select count(*) \
                from rtxn x \
               where x.rtxntypcd in (select x1.rtxntypcd \
                                       from rtxntyp x1 \
                                      where x1.rtxntypcatcd in ('SC', 'MSC') \
                                     union \
                                     select 'GLR' from dual) \
                 and exists (select 1 \
                        from rtxnentityattrib x2 \
                       where x2.acctnbr = x.acctnbr \
                         and x2.rtxnnbr = x.rtxnnbr \
                         and x2.entityattribcd = 'SCRT') \
                 and x.acctnbr = e.acctnbr \
                 and x.rtxnnbr = e.rtxnnbr), \
              0, \
              (decode((select 1 \
                        from rtxn t \
                       where ((t.parentacctnbr = e.acctnbr and \
                             t.parentrtxnnbr = e.rtxnnbr and \
                             t.rtxntypcd = 'WTH') or \
                             (t.acctnbr = e.parentacctnbr and \
                             t.rtxnnbr = e.parentrtxnnbr and \
                             t.rtxntypcd = 'DEP')) \
                         and t.applnbr = 1011), \
                      1, \
                      noiDung || ' ' || ' TU TK SO ' || ' \
                                    ' || \
                      (decode(acctnbrWTH, null, acctnbr, acctnbrWTH)), \
                      noiDung)), \
              ' THU PHI GIAO DICH ') AS \"descText\" \
  from (select * from (select  rownum \"rowId\", rtxnnbr, \
               thoiGianGiaoDich, \
               nvl(decode(instr(intrtxndesctext, '#'), \
                          0, \
                          intrtxndesctext, \
                          upper(substr(intrtxndesctext, \
                                       0, \
                                       instr(intrtxndesctext, '#') - 1) || ' \
                                              ' || \
                                substr(intrtxndesctext, \
                                       instr(intrtxndesctext, '#', 1, 2) + 1, \
                                       length(intrtxndesctext) - \
                                       instr(intrtxndesctext, '#', 1, 2) - 1) || \
                                extrtxndesctext)), \
                   ' ') noiDung, \
               soTien, \
               acctnbr, \
               parentacctnbr, \
               parentrtxnnbr, \
               (select d1.acctnbr \
                  from rtxn d1 \
                 where d1.parentacctnbr = d.acctnbr \
                   and d1.parentrtxnnbr = d.rtxnnbr \
                   and d1.rtxntypcd = 'WTH' \
                   and d1.applnbr = 1011) as acctnbrWTH \
          from (select a.rtxnnbr, \
                       to_char(origpostdate, 'YYYY-MM-DD') thoiGianGiaoDich, \
                       (select b.intrtxndesctext \
                          from intrtxndesc b \
                         where b.intrtxndescnbr = a.intrrtxndescnbr) as intrtxndesctext, \
                       (select c.extrtxndesctext \
                          from extrtxndesc c \
                         where c.extrtxndescnbr = a.extrtxndescnbr) as extrtxndesctext, \
                       tranamt soTien, \
                       a.acctnbr, \
                       a.parentacctnbr, \
                       a.parentrtxnnbr \
                  from rtxn a " + sql_rtxnref_table + "  \
                 where  \
                  " + sql_rtxnref_where + "  \
         rtxntypcd not in ('IAA', 'PEN') \
                   and a.acctnbr = :accountnbr \
                   and a.origpostdate >= to_date(:fromdate, 'YYYY-MM-DD') \
                   and a.origpostdate <= to_date(:todate, 'YYYY-MM-DD') \
                    " + sql_rtxnref_para + "  \
                     and rownum <=:row_end  \
        order by a.origpostdate , a.rtxnnbr ) d) ) e where \"rowId\">:row_begin  \
"

        if bl_docref is False:
            # print('tai khoan THUONG KHONG dung bl_docref')
            params = {"accountnbr": str(accountnbr), "fromdate": fromdate, "todate": todate,"row_begin": row_begin,"row_end": row_end}
        else:
            # print('tai khoan THUONG dung bl_docref')
            params = {"accountnbr": str(accountnbr), "fromdate": fromdate, "todate": todate, "rtxnref": rtxnref, "row_begin": row_begin,"row_end": row_end}

    ret = sqltojson(connection, sqltext, params=params )

    next_page= 1
    if(len(ret)< in_size):
        next_page=0

    info = {
        'page': in_page,
        'size': in_size,
        'nextPage': next_page

    }

    transactions_results.append({"details": ret})
    transactions_results.append({"pageInfo": info})

    return transactions_results




