from flask import jsonify
import cx_Oracle
import config
from common import exception
from common import oracle_connection, connection_pool, exception
from oe.ld.account import account_availbal


def account_passbook_available(connection, accountnbr, serial_number):
    print('called account_passbook_available')


    cursor = connection.cursor()

    out_mjaccttypcd = cursor.var(str)
    out_miaccttypcd = cursor.var(str)
    out_mjmiaccttypdesc = cursor.var(str)
    out_currencycd = cursor.var(str)
    out_currstatcd = cursor.var(str)

    out_contractdate = cursor.var(cx_Oracle.DATETIME)
    out_maturitydate = cursor.var(cx_Oracle.DATETIME)

    out_passbookseri = cursor.var(str)

    out_errornbr = cursor.var(str)
    out_errormsg = cursor.var(str)
    out_oraerrormsg = cursor.var(str)

    in_acctnbr = accountnbr





    cursor.callproc('pack_core_api_acctdtl.proc_getpassbookinfo', (in_acctnbr, \
                                                                   out_mjaccttypcd, \
                                                                   out_miaccttypcd, \
                                                                   out_mjmiaccttypdesc, \
                                                                   out_currencycd, \
                                                                   out_currstatcd, \
                                                                   out_contractdate, \
                                                                   out_maturitydate, \
                                                                   out_passbookseri, \
                                                                   out_errornbr, \
                                                                   out_errormsg, \
                                                                   out_oraerrormsg))


    print("cursor close")
    # print(out_currencycd)
    cursor.close()
    m_errornbr = out_errornbr.getvalue()
    if m_errornbr is None:
        m_errornbr = ''
    # print("m_errornbr  >"+m_errornbr)
    if len(m_errornbr) > 0 and m_errornbr != '0':
        # print("PROC_AVAILFUNDS>error code: "+ m_errornbr)
        msg = "account not found: %s" % (accountnbr)
        raise exception.ESBNotFound(int(m_errornbr), msg)
        # return  make_response(jsonify({"errorCode": m_errornbr,"message": "NOT FOUND"}),400)

    print("out_passbookseri>" + out_passbookseri.getvalue())
    if out_passbookseri.getvalue() == serial_number:
        ret = account_availbal.account_available(connection, accountnbr)

        ret = {
            'availAmt': ret.get("availAmt"),
            'currencyType': out_currencycd.getvalue(),
        }
        return ret

    raise exception.ESBNotFound(404, 'seri is not found')


