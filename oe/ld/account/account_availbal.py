from flask import jsonify
import cx_Oracle
import config
from common import exception
from common import oracle_connection, connection_pool, exception

def  account_available_balance(connection, accountnbr):    
    """ lay thong tin tai khoan : full thong tin """
    cursor = connection.cursor()
    out_acctbalamt = cursor.var(float)
    out_acctavailamt = cursor.var(float)
    out_acctmgmtholdamt = cursor.var(float)
    out_acctavailholdamt = cursor.var(float)
    out_totdrawamt = cursor.var(float)
    out_totcreditavailamt = cursor.var(float)
    out_totcreditlimitamt = cursor.var(float)
    out_errornbr = cursor.var(str)
    out_errormsg = cursor.var(str)
    out_oraerrormsg = cursor.var(str)

    in_acctnbr = accountnbr #
    in_notesubacctnbr = 'BAL' #notesubacctnbr # 'BAL'
    in_subacctnbr = -1 # 1
    in_effdate = None
    in_availmethcd = 'ONLI'
    in_debugyn = 'N'

    sql="SELECT \
        (SELECT nvl((select b.bankoptionvalue from bankoption b where b.bankoptioncd = 'CDAT' and b.bankoptiononyn = 'Y'), \
        (SELECT t.bankoptionvalue from bankoption t where t.bankoptioncd = 'PDAT')) from dual) EFFDATE, \
    (SELECT SUBACCTNBR FROM ACCTSUBACCT WHERE ACCTNBR =:in_acctnbr \
    AND BALCATCD = 'NOTE' AND BALTYPCD =:in_notesubacctnbr) SUBACCTNBR FROM DUAL"
    # print (sql)

    # Start Debug
    # print ("debug")
    ########sql = "select 1 / 0 from dual"
    # End Debug
    cursor.execute(sql, {"in_acctnbr":str(in_acctnbr), "in_notesubacctnbr":in_notesubacctnbr})

    row = cursor.fetchone()
    if row is None:
        raise exception.ESBNotFound(404, "SUBACCTNBR not found for account %s"%accountnbr)

    in_effdate = row[0]
    in_subacctnbr = row[1]

    #test debug
    ###############in_effdate='10-1-1'
    # end test debug
    #cursor.callproc('PROC_AVAILFUNDS',
    # cursor.callproc('ESI_APP_proc_AVAILFUNDS'
    cursor.callproc('PROC_AVAILFUNDS', (in_acctnbr, in_subacctnbr, in_effdate, in_availmethcd,  in_debugyn, \
        out_acctbalamt, \
        out_acctavailamt, \
        out_acctmgmtholdamt, \
        out_acctavailholdamt, \
        out_totdrawamt, \
        out_totcreditavailamt, \
        out_totcreditlimitamt, \
        out_errornbr, \
        out_errormsg, \
        out_oraerrormsg))


    print("cursor close")
    cursor.close()
    m_errornbr = out_errornbr.getvalue()
    if m_errornbr is None:
        m_errornbr = ''
    # print("m_errornbr  >"+m_errornbr)
    if len(m_errornbr) > 0  :
        # print("PROC_AVAILFUNDS>error code: "+ m_errornbr)
        msg = "account not found: %s" % (accountnbr)
        raise exception.ESBNotFound(int(m_errornbr), msg)
        # return  make_response(jsonify({"errorCode": m_errornbr,"message": "NOT FOUND"}),400)

    ret = {
        'acctBalAmt': round(out_acctbalamt.getvalue(), 2),
        'acctAvailAmt': round(out_acctavailamt.getvalue(), 2),
        'acctMgmtHoldAmt': round(out_acctmgmtholdamt.getvalue(), 2),
        'acctAvailHoldAmt': round(out_acctavailholdamt.getvalue(), 2),
        'totDrawAmt': round(out_totdrawamt.getvalue(), 2),
        'totCreditAvailAmt': round(out_totcreditavailamt.getvalue(), 2),
        'totCreditLimitAmt': round(out_totcreditlimitamt.getvalue(), 2)
        }
    return ret



def account_available(connection, accountnbr):
    """ lay thong tin tai khoan - chi lay 'availAmt' """
    ret = account_available_balance(connection, accountnbr)

    ret = {
         'availAmt': ret.get("acctAvailAmt"),
        }
    return ret



# mypoolDna = connection_pool.mypoolDna
#
# # ham nay la sample tu quan ly connection se duoc di tu cong simple_rest_api_wrapper
# def account_available_balance_port(accountnbr):
#     connection = None
#     ret = None
#     try:
#         connection = mypoolDna.acquire()
#     except:  # cx_Oracle.DatabaseError as e:
#         print("get_Connection Cannot open connection ")
#         raise exception.ESBDBConnectionError("Cannot open connection", config.DB_CONNECTION_ERROR)
#
#     if connection is None:
#         print("connection is None ")
#         raise exception.ESBDBConnectionError("Cannot open connection", config.DB_CONNECTION_ERROR)
#
#
#     try:
#
#         ret = account_available_balance(accountnbr , connection)
#
#         print("release  connection")
#         mypoolDna.release(connection)
#     except cx_Oracle.DatabaseError as e:
#         print("DatabaseError ")
#         print("drop  connection")
#         mypoolDna.drop(connection)
#         raise e
#     except Exception as e:
#         print("Exception ")
#         print("release  connection")
#         mypoolDna.release(connection)
#         raise e
#     return ret
#

