from flask_caching import Cache
import config

if config.DEBUG is False:
    app_cache = Cache(config={'CACHE_TYPE': 'redis', 'CACHE_REDIS_HOST': config.CACHE_REDIS_HOST, 'CACHE_REDIS_PORT': config.CACHE_REDIS_PORT})
else:
    app_cache = Cache(config={'CACHE_TYPE': 'simple'})