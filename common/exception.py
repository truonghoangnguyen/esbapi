class ESBError(Exception):
    """Base class for exceptions in this module."""
    def __init__(self, code, message):
        self.code = code
        self.message = message


class ESBDBConnectionError(ESBError):
    pass

class ESBNotFound(ESBError):
    pass

class ESBBadRequest(ESBError):
    pass