import cx_Oracle
from common import exception
from flask import make_response, jsonify
from common import oracle_connection, connection_pool, exception
import config



mypoolDna = connection_pool.mypoolDna


def oracedb_rest_api_wrapper(fname, *kargs, **kwargs):
    connection = None
    ret = None
    ret_error = None
    b_release_connection = True
    try:
        try:
            connection = mypoolDna.acquire()
        except Exception as e:  # cx_Oracle.DatabaseError as e:
            print("get_Connection Cannot open connection ")
            raise exception.ESBDBConnectionError(config.DB_CONNECTION_ERROR, "Cannot open connection %s"%str(e) )

        if connection is None:
            print("connection is None ")
            raise exception.ESBDBConnectionError(config.DB_CONNECTION_ERROR,"Cannot open connection")

        ret = fname(connection, *kargs, **kwargs)

        print("200 OK release  connection")
        mypoolDna.release(connection)

        return jsonify(ret)

        # Oracle db connection error
    except cx_Oracle.DatabaseError as e:
        error, = e.args
        session_killed = 28

        print("Oracle-Error-Message:", error.message)
        print("drop  connection")
        mypoolDna.drop(connection)
        b_release_connection = False

        if error.code == session_killed:
            ret_error = make_response(
                jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError session_killed"}), 500)
        elif error.code == 12543 or error.code == 3113:
            ret_error = make_response(
                jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError Disconnect to DB server"}), 500)
        else:
            ret_error = make_response(jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError Unknown > " + error.message}),
                                      500)

    # ACB defined error
    except exception.ESBNotFound as e:
        print("ESBNotFound ")
        ret_error = make_response(jsonify({"errorCode": e.code, "message": e.message}), 404)
    except exception.ESBDBConnectionError as e:
        # khong tao duoc connection thanh cong
        print("ESBDBConnectionError ")
        b_release_connection = False
        ret_error = make_response(jsonify({"errorCode": e.code, "message": e.message}), 500)
    except exception.ESBBadRequest as e:
        print("ESBBadRequest ")
        ret_error = make_response(jsonify({"errorCode": e.code, "message": e.message}), 400)
    # commond
    except Exception as e:
        print("Exception %s"%str(e))
        ret_error = make_response(jsonify({"errorCode": 9090, "message": str(e)}), 500)

    try:

        print("Exception ")

        if b_release_connection:
            print("release connection in Exception case ")
            mypoolDna.release(connection)
    except Exception as e:
        print("cannot release  connection")
    return ret_error




#wrap chi dung dna pool
# def dna_rest_api_wrapper(fname, *kargs, **kwargs):
#     return oracedb_rest_api_wrapper(mypoolDna,fname,*kargs, **kwargs)
#
#
# def oracedb_rest_api_wrapper(mypool, fname, *kargs, **kwargs):
#     connection = None
#     ret = None
#     ret_error = None
#     b_release_connection = True
#
#     try:
#         try:
#             connection = mypool.acquire()
#         except:  # cx_Oracle.DatabaseError as e:
#             print("get_Connection Cannot open connection ")
#             raise exception.ESBDBConnectionError("Cannot open connection", config.DB_CONNECTION_ERROR)
#
#         if connection is None:
#             print("connection is None ")
#             raise exception.ESBDBConnectionError("Cannot open connection", config.DB_CONNECTION_ERROR)
#
#         ret = fname(connection, *kargs, **kwargs)
#
#         print("200 OK release  connection")
#         mypool.release(connection)
#
#         return jsonify(ret)
#
#         # Oracle db connection error
#     except cx_Oracle.DatabaseError as e:
#         error, = e.args
#         session_killed = 28
#
#         print("DatabaseError ")
#         print("drop  connection")
#         mypool.drop(connection)
#         b_release_connection = False
#
#         if error.code == session_killed:
#             ret_error = make_response(
#                 jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError session_killed"}), 500)
#         elif error.code == 12543 or error.code == 3113:
#             ret_error = make_response(
#                 jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError Disconnect to DB server"}), 500)
#         else:
#             ret_error = make_response(jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError Unknown"}),
#                                       500)
#
#     # ACB defined error
#     except exception.ESBNotFound as e:
#         print("ESBNotFound ")
#         ret_error = make_response(jsonify({"errorCode": e.code, "message": e.message}), 404)
#     except exception.ESBDBConnectionError as e:
#         # khong tao duoc connection thanh cong
#         print("ESBDBConnectionError ")
#         b_release_connection = False
#         ret_error = make_response(jsonify({"errorCode": e.code, "message": e.message}), 500)
#     except exception.ESBBadRequest as e:
#         print("ESBBadRequest ")
#         ret_error = make_response(jsonify({"errorCode": e.code, "message": e.message}), 400)
#     # commond
#     except Exception as e:
#         print("Exception ")
#         ret_error = make_response(jsonify({"errorCode": 9090, "message": str(e)}), 500)
#
#     try:
#
#         print("Exception ")
#
#         if b_release_connection:
#             print("release connection in Exception case ")
#             mypool.release(connection)
#     except Exception as e:
#         print("cannot release  connection")
#     return ret_error
#

# Dung khi ham xu ly tu quan ly connection pool
# def simple_rest_api_wrapper(fname, *kargs, **kwargs):
#     try:
#         ret = fname(*kargs, **kwargs)
#         return jsonify(ret)
#
#         # Oracle db connection error
#     except cx_Oracle.DatabaseError as e:
#         error, = e.args
#         session_killed = 28
#         if error.code == session_killed:
#             return make_response(
#                 jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError session_killed"}), 500)
#         elif error.code == 12543 or error.code == 3113:
#             return make_response(
#                 jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError Disconnect to DB server"}), 500)
#         else:
#             return make_response(jsonify({"errorCode": error.code, "message": "cx_Oracle.DatabaseError Unknown"}), 500)
#
#     # ACB defined error
#     except exception.ESBNotFound as e:
#         return make_response(jsonify({"errorCode": e.code, "message": e.message}), 404)
#     except exception.ESBDBConnectionError as e:
#         return make_response(jsonify({"errorCode": e.code, "message": e.message}), 500)
#
#     # commond
#     except Exception as e:
#         return make_response(jsonify({"errorCode": 9090, "message": str(e)}), 500)