import json
from common import exception


def  sqltojson(connection, query, params=None, raiseerror=True, closecursor=True):
    print(query)
    cursor = connection.cursor()
    # print('execute')
    if not params:
        print('not params')
        result = cursor.execute(query)
    else:
        #cursor.execute("INSERT INTO table VALUES (%s, %s, %s)", (var1, var2, var3))
        print('has params')
        result = cursor.execute(query, params)
    # result = cursor.execute('select * from dual')
    print('get data')
    items = [dict(zip([key[0] for key in cursor.description], row)) for row in result]
    # print(json.dumps({"items": items}))
    if closecursor is True:
        cursor.close()

    if raiseerror is True and len(items) == 0:
        raise exception.ESBNotFound(404, "No Data Found")
    return items

def  sqltojson_one(connection, query, params=None, raiseerror=True, closecursor=True):

    items = sqltojson(connection, query, params=params, raiseerror=False, closecursor=closecursor)
    if raiseerror is True and len(items) == 0:
        raise exception.ESBNotFound(404, "No Data Found")
    return items[0]

