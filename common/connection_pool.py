import cx_Oracle
import config


def Create_SessionPool():
    """ DNA connection pool. one-time pool !
    """
    print("Create_SessionPool")
    my = cx_Oracle.SessionPool(user = config.MAIN_USER,
        password=config.MAIN_PASSWORD,
        dsn = config.CONNECT_STRING,
        min = config.MINCONNECTIONS,
        max = config.MAXCONNECTIONS,
        increment = config.INCREMENT,
        connectiontype = cx_Oracle.Connection,
        threaded = True,
        getmode = cx_Oracle.SPOOL_ATTRVAL_NOWAIT,
        homogeneous=True)
    my.timeout = 600

    return my


mypoolDna = Create_SessionPool()
