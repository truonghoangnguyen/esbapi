from flask import Flask, jsonify, abort
from flask_caching import Cache
import config
from oe.ld.account import account_api
from services import services_api
from flasgger import Swagger

from services import services_api
from common.cache import app_cache

template = dict(
    info={
        'title': 'ACB ESB API HOME SPACE',
        'version': '0.1',
        'description': 'Operations & Execution'        
    }    
)

app = Flask(__name__)
app_cache.init_app(app)
swagger = Swagger(app, template=template)


app.register_blueprint(account_api.account_api)
app.register_blueprint(services_api.service_healthcheck)

if __name__ == '__main__':
    app.run(debug=config.DEBUG)
