


def service_healthchecks(connection):

    cursor = connection.cursor()
    out_sysdate = None

    sql="SELECT TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS') FROM DUAL "

    cursor.execute(sql)
    # results = cursor.fetchall()
    # for row in results:
    #     out_sysdate = row[0]

    row = cursor.fetchone()
    out_sysdate = row[0]


    cursor.close()

    ret = {

        'sysdate': out_sysdate
        }
    return ret

