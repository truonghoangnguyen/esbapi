from services import healthcheck
from common.api_handle import oracedb_rest_api_wrapper
from flask import jsonify, Blueprint

service_healthcheck = Blueprint('service_healthcheck', __name__)

@service_healthcheck.route('/services/healthchecks')
def service_healthchecks():
    return oracedb_rest_api_wrapper(healthcheck.service_healthchecks)

@service_healthcheck.route('/services/check')
def service_check():
    # return jsonify({"m":"ok"})
    return "ok"


